var express = require('express');
var router = express.Router();
var cors = require('cors')
const axios = require('axios');

const urlpath = "https://www.facilitase.com.br/";

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get("/categorias", async (req, res, next) => {
	const idretorno = req.query.id;
	try {
		await axios.get(urlpath+'api/catalog_system/pub/category/tree/',{params: {id: idretorno}})
		.then(response => res.send(response.data))
	}
	catch (err) {
		next(err)
	}
})

module.exports = router;
